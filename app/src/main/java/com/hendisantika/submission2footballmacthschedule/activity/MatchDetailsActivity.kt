package com.hendisantika.submission2footballmacthschedule.activity

import android.annotation.SuppressLint
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hendisantika.submission2footballmacthschedule.R
import com.hendisantika.submission2footballmacthschedule.R.drawable.ic_add_to_favorites
import com.hendisantika.submission2footballmacthschedule.R.drawable.ic_added_to_favorites
import com.hendisantika.submission2footballmacthschedule.R.id.*
import com.hendisantika.submission2footballmacthschedule.R.menu.detail_menu
import com.hendisantika.submission2footballmacthschedule.api.SportDBApi
import com.hendisantika.submission2footballmacthschedule.db.Favorite
import com.hendisantika.submission2footballmacthschedule.db.database
import com.hendisantika.submission2footballmacthschedule.entity.Events
import com.hendisantika.submission2footballmacthschedule.presenter.MatchDetailsPresenter
import com.hendisantika.submission2footballmacthschedule.utils.invisible
import com.hendisantika.submission2footballmacthschedule.utils.visible
import com.hendisantika.submission2footballmacthschedule.view.MatchDetailsActivityView
import com.hendisantika.submission2footballmacthschedule.view.MatchDetailsView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.support.v4.onRefresh
import rx.android.schedulers.AndroidSchedulers
import java.text.SimpleDateFormat

/**
 * Created by hendisantika on 21/10/18  07.16.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchDetailsActivity : AppCompatActivity(), MatchDetailsView {
    private lateinit var presenter: MatchDetailsPresenter
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var eventId: String

    private var eventDate: String? = null
    private var homeName: String? = null
    private var homeScore: String? = null
    private var awayName: String? = null
    private var awayScore: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MatchDetailsActivityView().setContentView(this)

        val intent = intent
        eventId = intent.getStringExtra("id_event")

        progressBar = find(R.id.progressBar)
        swipeRefresh = find(R.id.swipeRefresh)

        favoriteState()
        presenter = MatchDetailsPresenter(this, SportDBApi.OKHttpNich(), AndroidSchedulers.mainThread())
        presenter.getEventData(intent.getStringExtra("id_event"))

        swipeRefresh.onRefresh {
            presenter.getEventData(intent.getStringExtra("id_event"))
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun setTeamBadge(home: String?, away: String?) {
        swipeRefresh.isRefreshing = false

        val homeImg: ImageView = find(R.id.imgHome)
        val awayImg: ImageView = find(R.id.imgAway)

        Glide.with(this).load(home).into(homeImg)
        Glide.with(this).load(away).into(awayImg)
    }

    override fun setEventData(data: List<Events>) {
        eventDate = data[0].eventDate
        homeName = data[0].homeTeam
        homeScore = data[0].homeScore
        awayName = data[0].awayTeam
        awayScore = data[0].awayScore

        val homeName: TextView = find(home_team)
        val homeScore: TextView = find(home_score)
        val homeGoal: TextView = find(R.id.home_goal)
        val homeRedCards: TextView = find(R.id.home_red_cards)
        val homeYellowCards: TextView = find(R.id.home_yellow_cards)

        val awayName: TextView = find(away_team)
        val awayScore: TextView = find(away_score)
        val awayGoal: TextView = find(R.id.away_goal)
        val awayRedCards: TextView = find(R.id.away_red_cards)
        val awayYellowCards: TextView = find(R.id.away_yellow_cards)
        val eventDate: TextView = find(R.id.event_date)

        homeName.text = data[0].homeTeam
        homeScore.text = data[0].homeScore
        homeGoal.text = setData(data[0].homeGoalDetails)
        homeRedCards.text = setData(data[0].homeRedCards)
        homeYellowCards.text = setData(data[0].homeYellowCards)

        awayName.text = data[0].awayTeam
        awayScore.text = data[0].awayScore
        awayGoal.text = setData(data[0].awayGoalDetails)
        awayRedCards.text = setData(data[0].awayRedCards)
        awayYellowCards.text = setData(data[0].awayYellowCards)

        eventDate.text = parseDateTime(data[0].eventDate)
    }

    private fun setData(data: String?): String {
        return data?.replace(";", "\n") ?: resources.getString(R.string.no_data)
    }

    @SuppressLint("SimpleDateFormat")
    private fun parseDateTime(date: String?): String {
//        val formatedDate = SimpleDateFormat("EEE, dd/MM/yy").parse(date)
        val formattedDate = SimpleDateFormat("dd/MM/yy").parse(date)
        val rDate = SimpleDateFormat("EE, dd-MM-yyyy")
//        return formattedDate.toString()
        return rDate.format(formattedDate)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()



                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(Favorite.TABLE_FAVORITE)
                .whereArgs(
                    "(EVENT_ID = {id})",
                    "id" to eventId
                )
            val favorite = result.parseList(classParser<Favorite>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun addToFavorite() {
        try {

            if (eventDate != null) {

                database.use {
                    insert(
                        Favorite.TABLE_FAVORITE,
                        Favorite.EVENT_ID to eventId,
                        Favorite.EVENT_DATE to parseDateTime(eventDate),
                        Favorite.HOME_NAME to homeName,
                        Favorite.HOME_SCORE to homeScore,
                        Favorite.AWAY_NAME to awayName,
                        Favorite.AWAY_SCORE to awayScore
                    )
                }

                isFavorite = !isFavorite
                setFavorite()

                snackbar(swipeRefresh, "Added to favorite").show()
            }

        } catch (e: SQLiteConstraintException) {
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(
                    Favorite.TABLE_FAVORITE, "(EVENT_ID = {id})",
                    "id" to eventId
                )
            }

            isFavorite = !isFavorite
            setFavorite()

            snackbar(swipeRefresh, "Removed from favorite").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unSubscribe()
    }
}