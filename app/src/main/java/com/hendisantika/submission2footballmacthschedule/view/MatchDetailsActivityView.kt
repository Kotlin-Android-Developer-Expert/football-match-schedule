package com.hendisantika.submission2footballmacthschedule.view

import android.graphics.Color
import android.view.Gravity
import com.hendisantika.submission2footballmacthschedule.R
import com.hendisantika.submission2footballmacthschedule.activity.MatchDetailsActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class MatchDetailsActivityView : AnkoComponent<MatchDetailsActivity> {
    override fun createView(ui: AnkoContext<MatchDetailsActivity>) = with(ui) {

        swipeRefreshLayout {
            id = R.id.swipeRefresh

            scrollView {
                lparams(width = matchParent, height = matchParent)

                verticalLayout {
                    lparams(width = matchParent, height = matchParent)
                    padding = dip(16)

                    textView {
                        id = R.id.event_date
                        textColor = Color.BLUE
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams(width = matchParent, height = wrapContent)

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            topMargin = dip(16)
                        }

                        progressBar {
                            id = R.id.progressBar
                        }.lparams {
                            centerInParent()
                        }

                        relativeLayout {
                            lparams(width = wrapContent, height = wrapContent)

                            imageView {
                                id = R.id.imgHome
                            }.lparams(width = dip(50), height = dip(50)) {
                                centerHorizontally()
                            }

                            textView {
                                id = R.id.home_score
                                textColor = Color.BLUE
                                textSize = 18F
                            }.lparams {
                                centerHorizontally()
                                below(R.id.imgHome)
                                topMargin = dip(8)
                            }

                            textView {
                                id = R.id.home_team
                                textColor = Color.BLUE
                            }.lparams {
                                centerHorizontally()
                                below(R.id.home_score)
                            }
                        }

                        textView {
                            id = R.id.versus
                            text = resources.getString(R.string.vs)
                            textSize = 24F
                            textColor = Color.BLUE
                        }.lparams {
                            centerInParent()
                        }

                        relativeLayout {
                            lparams(width = wrapContent, height = wrapContent)

                            imageView {
                                id = R.id.imgAway
                            }.lparams(width = dip(50), height = dip(50)) {
                                centerHorizontally()
                            }

                            textView {
                                id = R.id.away_score
                                textColor = Color.BLUE
                                textSize = 18F
                            }.lparams {
                                centerHorizontally()
                                below(R.id.imgAway)
                                topMargin = dip(8)
                            }

                            textView {
                                id = R.id.away_team
                                textColor = Color.BLUE
                            }.lparams {
                                centerHorizontally()
                                below(R.id.away_score)
                            }
                        }.lparams {
                            alignParentRight()
                        }
                    }

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            topMargin = dip(8)
                        }

                        view {
                            backgroundColor = Color.BLUE
                        }.lparams(width = matchParent, height = dip(1)) {
                            leftOf(R.id.goal)
                            baselineOf(R.id.goal)
                        }

                        textView {
                            id = R.id.goal
                            text = resources.getString(R.string.goal)
                            textColor = Color.BLUE
                        }.lparams {
                            centerHorizontally()
                            leftMargin = dip(8)
                            rightMargin = dip(8)
                        }

                        view {
                            backgroundColor = Color.BLUE
                        }.lparams(width = matchParent, height = dip(1)) {
                            rightOf(R.id.goal)
                            baselineOf(R.id.goal)
                        }

                        textView {
                            id = R.id.home_goal
                            textColor = Color.BLUE
                        }.lparams {
                            below(R.id.goal)
                        }

                        textView {
                            id = R.id.away_goal
                            textColor = Color.BLUE
                        }.lparams {
                            below(R.id.goal)
                            alignParentRight()
                        }
                    }

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            topMargin = dip(8)
                        }

                        view {
                            backgroundColor = Color.BLUE
                        }.lparams(width = matchParent, height = dip(1)) {
                            leftOf(R.id.red_cards)
                            baselineOf(R.id.red_cards)
                        }

                        textView {
                            id = R.id.red_cards
                            text = resources.getString(R.string.redCards)
                            textColor = Color.BLUE
                        }.lparams {
                            centerHorizontally()
                            leftMargin = dip(8)
                            rightMargin = dip(8)
                        }

                        view {
                            backgroundColor = Color.BLUE
                        }.lparams(width = matchParent, height = dip(1)) {
                            rightOf(R.id.red_cards)
                            baselineOf(R.id.red_cards)
                        }

                        textView {
                            id = R.id.home_red_cards
                            textColor = Color.BLUE
                        }.lparams {
                            below(R.id.red_cards)
                        }

                        textView {
                            id = R.id.away_red_cards
                            textColor = Color.BLUE
                        }.lparams {
                            below(R.id.red_cards)
                            alignParentRight()
                        }
                    }

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            topMargin = dip(8)
                        }

                        view {
                            backgroundColor = Color.BLUE
                        }.lparams(width = matchParent, height = dip(1)) {
                            leftOf(R.id.yellow_cards)
                            baselineOf(R.id.yellow_cards)
                        }

                        textView {
                            id = R.id.yellow_cards
                            text = resources.getString(R.string.yellowCards)
                            textColor = Color.BLUE
                        }.lparams {
                            centerHorizontally()
                            leftMargin = dip(8)
                            rightMargin = dip(8)
                        }

                        view {
                            backgroundColor = Color.BLUE
                        }.lparams(width = matchParent, height = dip(1)) {
                            rightOf(R.id.yellow_cards)
                            baselineOf(R.id.yellow_cards)
                        }

                        textView {
                            id = R.id.home_yellow_cards
                            textColor = Color.BLUE
                        }.lparams {
                            below(R.id.yellow_cards)
                        }

                        textView {
                            id = R.id.away_yellow_cards
                            textColor = Color.BLUE
                        }.lparams {
                            below(R.id.yellow_cards)
                            alignParentRight()
                        }
                    }
                }
            }
        }


    }
}