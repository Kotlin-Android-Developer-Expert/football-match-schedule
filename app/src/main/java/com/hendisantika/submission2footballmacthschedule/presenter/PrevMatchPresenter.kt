package com.hendisantika.submission2footballmacthschedule.presenter

import android.util.Log
import com.hendisantika.submission2footballmacthschedule.api.SportDBApi
import com.hendisantika.submission2footballmacthschedule.view.PrevMatchView
import rx.Scheduler
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

/**
 * Created by hendisantika on 21/10/18  20.59.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class PrevMatchPresenter(
    private val view: PrevMatchView, private val sportDBApi: SportDBApi,
    private var scheduler: Scheduler
) {

    private val compositeSubs: CompositeSubscription = CompositeSubscription()

    fun getPastMatchesList(league: String) {
        view.showLoading()

        val subs = sportDBApi.getPastMatch(league)
            .subscribeOn(Schedulers.newThread())
            .observeOn(scheduler)
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.showMatchList(result.events)
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )
        compositeSubs.add(subs)
    }


    fun unSubscribe(){
        compositeSubs.unsubscribe()
    }
}