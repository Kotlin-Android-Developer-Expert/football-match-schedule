package com.hendisantika.submission2footballmacthschedule.api

import com.hendisantika.submission2footballmacthschedule.BuildConfig
import com.hendisantika.submission2footballmacthschedule.entity.Matches
import com.hendisantika.submission2footballmacthschedule.entity.Teams
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * Created by hendisantika on 21/10/18  06.36.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface SportDBApi {
    @GET("eventspastleague.php")
    fun getPastMatch(@Query("id") leagueId: String): Observable<Matches>

    @GET("eventsnextleague.php")
    fun getNextMatch(@Query("id") leagueId: String): Observable<Matches>

    @GET("lookupteam.php")
    fun getTeamDetails(@Query("id") teamId: String?): Observable<Teams>

    @GET("lookupevent.php")
    fun getEvent(@Query("id") eventId: String): Observable<Matches>

    companion object {
        fun OKHttpNich(): SportDBApi {
            val httpClient = OkHttpClient().newBuilder()
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggingInterceptor)

            val retrofit = Retrofit.Builder()
                .client(httpClient.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()
            return retrofit.create(SportDBApi::class.java)
        }
    }
}