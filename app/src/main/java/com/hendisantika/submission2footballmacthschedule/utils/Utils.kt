package com.hendisantika.submission2footballmacthschedule.utils

import android.view.View

/**
 * Created by hendisantika on 21/10/18  07.43.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}