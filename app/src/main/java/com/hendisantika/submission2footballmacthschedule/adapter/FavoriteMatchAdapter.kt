package com.hendisantika.submission2footballmacthschedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hendisantika.submission2footballmacthschedule.R
import com.hendisantika.submission2footballmacthschedule.R.id.*
import com.hendisantika.submission2footballmacthschedule.db.Favorite
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Created by hendisantika on 21/10/18  21.43.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class FavoriteMatchAdapter(
    private val favorite: List<Favorite>,
    private val listener: (Favorite) -> Unit
) : RecyclerView.Adapter<FavoriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder(MatchUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bindItem(favorite[position], listener)
    }
}

class MatchUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            cardView {
                lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(10)
                    leftMargin = dip(5)
                    rightMargin = dip(5)
                }

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(10)

                    textView {
                        id = favMatchDate
                        textColor = R.color.colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams(width = matchParent, height = wrapContent) {
                        bottomMargin = dip(10)
                    }

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        textView {
                            id = favHomeName
                            textSize = 18F
                        }.lparams {
                            alignParentLeft()
                        }

                        textView {
                            id = favHomeScore
                            textSize = 18F
                        }.lparams {
                            leftOf(favVs)
                        }

                        textView {
                            id = favVs
                            text = resources.getString(R.string.vs)
                        }.lparams {
                            marginStart = dip(8)
                            marginEnd = dip(8)
                            centerHorizontally()
                        }

                        textView {
                            id = favAwayScore
                            textSize = 18F
                        }.lparams {
                            rightOf(favVs)
                        }

                        textView {
                            id = favAwayName
                            textSize = 18F
                        }.lparams {
                            alignParentRight()
                        }
                    }
                }
            }
        }
    }
}

class FavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val matchDate: TextView = view.find(favMatchDate)
    private val homeName: TextView = view.find(favHomeName)
    private val homeScore: TextView = view.find(favHomeScore)
    private val awayName: TextView = view.find(favAwayName)
    private val awayScore: TextView = view.find(favAwayScore)

    fun bindItem(favorite: Favorite, listener: (Favorite) -> Unit) {
        matchDate.text = favorite.eventDate
        homeName.text = favorite.homeName
        homeScore.text = favorite.homeScore
        awayName.text = favorite.awayName
        awayScore.text = favorite.awayScore

        itemView.onClick { listener(favorite) }
    }
}